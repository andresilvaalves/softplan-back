package io.andresilvaalves.softplan.sexo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SexoDto {

    private String nome;
    private String descricao;
    private String Sigla;

}
