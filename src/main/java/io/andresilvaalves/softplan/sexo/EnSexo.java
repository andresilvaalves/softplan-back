package io.andresilvaalves.softplan.sexo;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public enum EnSexo {

    MASCULINO("Masculino", "M"),
    FEMININO("Feminino", "F"),
    OUTROS("Outros", "O");

    EnSexo(String descricao, String sigla) {
        this.descricao = descricao;
        this.sigla = sigla;
    }

    private final String sigla;
    private final String descricao;

    public static EnSexo findBy(@NonNull String sexo) {
        return Stream.of(EnSexo.values())
                     .filter(f -> StringUtils.equalsAnyIgnoreCase(sexo, f.name(), f.getSigla()))
                     .findFirst()
                     .orElse(null);
    }

    public String getDescricao() {
        return descricao;
    }

    public String getSigla() {
        return sigla;
    }
}

