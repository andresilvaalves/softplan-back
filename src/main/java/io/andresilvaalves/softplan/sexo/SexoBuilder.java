package io.andresilvaalves.softplan.sexo;

import lombok.NonNull;

public class SexoBuilder {

    public static SexoDto builderToDTO(@NonNull EnSexo sexo) {
        return SexoDto.builder()
                      .nome(sexo.name())
                      .descricao(sexo.getDescricao())
                      .Sigla(sexo.getSigla())
                      .build();
    }

}
