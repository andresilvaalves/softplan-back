package io.andresilvaalves.softplan;

import io.andresilvaalves.softplan.pessoa.Pessoa;
import io.andresilvaalves.softplan.pessoa.PessoaRepository;
import io.andresilvaalves.softplan.sexo.EnSexo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.Month;

@Slf4j
@SpringBootApplication
public class SoftplanApplication {

    @Value("${app.message}")
    private String appMessage;

    @Autowired
    private PessoaRepository pessoaRepository;

    public static void main(String[] args) {
        SpringApplication.run(SoftplanApplication.class, args);
    }

    @Bean
    public CommandLineRunner run() {
        return args -> {

            Pessoa pessoa = new Pessoa();
            pessoa.setCpf("004.332.079-14");
            pessoa.setNome("André Silva Alves");
            pessoa.setEmail("andre.alves@softplan.com.br");
            pessoa.setNacionalidade("Brasileiro");
            pessoa.setNaturalidade("Linda Floripa");
            pessoa.setSexo(EnSexo.MASCULINO);
            pessoa.setNascimento(LocalDate.of(1981, Month.FEBRUARY, 26));

            pessoaRepository.save(pessoa);

            log.info(appMessage);
        };
    }

}
