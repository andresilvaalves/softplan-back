package io.andresilvaalves.softplan.config.security;

public class SecurityConstants {

	public static final String SECRET = "softplansecret";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 dias
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String LOGIN_URL = "/login";
}
