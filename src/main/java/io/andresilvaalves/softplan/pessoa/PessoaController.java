package io.andresilvaalves.softplan.pessoa;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @GetMapping
    public Page<PessoaDto> findAll(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size) {
        return pessoaService.findAll(page, size);
    }

    @GetMapping("{uuid}")
    public PessoaDto findById(@PathVariable("uuid") String uuid) {
        return pessoaService.findById(UUID.fromString(uuid));
    }

    @PostMapping
    public PessoaDto save(@NonNull @RequestBody PessoaDto pessoa) {
        return pessoaService.save(pessoa);
    }

    @PutMapping
    public PessoaDto update(@NonNull @RequestBody PessoaDto dto) {
        return pessoaService.update(dto);
    }

    @DeleteMapping("{uuid}")
    public void delete(@PathVariable("uuid") String uuid) {
        pessoaService.delete(UUID.fromString(uuid));
    }

}
