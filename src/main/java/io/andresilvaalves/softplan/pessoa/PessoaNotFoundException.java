package io.andresilvaalves.softplan.pessoa;

public class PessoaNotFoundException extends RuntimeException {

    public PessoaNotFoundException(String s) {
        super(s);
    }
}
