package io.andresilvaalves.softplan.pessoa;

import io.andresilvaalves.softplan.sexo.SexoDto;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
public class PessoaDto {

    private UUID uuid;

    @NonNull
    private String nome;

    private SexoDto sexo;

    private String email;

    private LocalDate nascimento;

    private String naturalidade;

    private String nacionalidade;

    @NonNull
    private String cpf;

}
