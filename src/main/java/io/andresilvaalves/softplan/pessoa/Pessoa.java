package io.andresilvaalves.softplan.pessoa;

import io.andresilvaalves.softplan.common.SoftPlanEntity;
import io.andresilvaalves.softplan.sexo.EnSexo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Pessoa extends SoftPlanEntity {

    @NotNull(message = "Nome obrigatório")
    @Column(name = "nome", nullable = false)
    private String nome;

    @Enumerated(EnumType.STRING)
    private EnSexo sexo;

    @Email(message = "email inválido")
    private String email;

    @NotNull(message = "nascimento obrigatório")
    @Column(name = "nascimento", nullable = false)
    private LocalDate nascimento;

    @Column(name = "naturalidade")
    private String naturalidade;

    @Column(name = "nacionalidade")
    private String nacionalidade;

    @NotNull(message = "CPF obrigatório")
    @Column(name = "cpf", nullable = false, unique = true)
    @CPF(message = "CPF inválido")
    private String cpf;

}
