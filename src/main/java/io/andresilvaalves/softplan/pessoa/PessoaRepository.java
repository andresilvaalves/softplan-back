package io.andresilvaalves.softplan.pessoa;

import org.springframework.data.repository.PagingAndSortingRepository;
import java.util.UUID;

public interface PessoaRepository extends PagingAndSortingRepository<Pessoa, UUID> {

}
