package io.andresilvaalves.softplan.pessoa;

import io.andresilvaalves.softplan.pessoa.Pessoa;
import io.andresilvaalves.softplan.pessoa.PessoaBuilder;
import io.andresilvaalves.softplan.pessoa.PessoaDto;
import io.andresilvaalves.softplan.pessoa.PessoaRepository;
import io.andresilvaalves.softplan.pessoa.PessoaNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PessoaService {

    private final PessoaRepository pessoaRepository;

    public Page<PessoaDto> findAll(Integer page, Integer size){
        Page<Pessoa> pessoas = pessoaRepository.findAll(PageRequest.of(page, size));
        return pessoas.map(PessoaBuilder::builderToDTO);
    }

    public PessoaDto findById(@NonNull UUID uuid) {
        Pessoa pessoa = pessoaRepository.findById(uuid).orElseThrow(() -> new PessoaNotFoundException(uuid.toString()));
        return PessoaBuilder.builderToDTO(pessoa);
    }

    public PessoaDto save(@NonNull PessoaDto dto) {
        Pessoa pessoa = new Pessoa();
        PessoaBuilder.builderToEntity(pessoa, dto);
        Pessoa saved = pessoaRepository.save(pessoa);
        return PessoaBuilder.builderToDTO(saved);
    }

    public PessoaDto update(@NonNull PessoaDto dto) {
        Pessoa pessoa = pessoaRepository.findById(dto.getUuid()).orElseThrow(() -> new PessoaNotFoundException(dto.getUuid().toString()));

        PessoaBuilder.builderToEntity(pessoa, dto);
        Pessoa saved = pessoaRepository.save(pessoa);
        return PessoaBuilder.builderToDTO(saved);
    }

    public void delete(@NonNull UUID uuid) {
        pessoaRepository.deleteById(uuid);
    }

}
