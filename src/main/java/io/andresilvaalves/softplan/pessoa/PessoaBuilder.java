package io.andresilvaalves.softplan.pessoa;

import io.andresilvaalves.softplan.sexo.EnSexo;
import io.andresilvaalves.softplan.sexo.SexoBuilder;
import lombok.NonNull;

public class PessoaBuilder {

    public static PessoaDto builderToDTO(@NonNull final Pessoa pessoa) {
        return PessoaDto.builder()
                        .uuid(pessoa.getUuid())
                        .nome(pessoa.getNome())
                        .cpf(pessoa.getCpf())
                        .email(pessoa.getEmail())
                        .nacionalidade(pessoa.getNacionalidade())
                        .naturalidade(pessoa.getNaturalidade())
                        .nascimento(pessoa.getNascimento())
                        .sexo(SexoBuilder.builderToDTO(pessoa.getSexo()))
                        .build();
    }

    public static void builderToEntity(@NonNull Pessoa pessoa, @NonNull final PessoaDto dto) {

        pessoa.setNome(dto.getNome());
        pessoa.setCpf(dto.getCpf());
        pessoa.setEmail(dto.getEmail());
        pessoa.setNacionalidade(dto.getNacionalidade());
        pessoa.setNaturalidade(dto.getNaturalidade());
        pessoa.setNascimento(dto.getNascimento());
        pessoa.setSexo(EnSexo.findBy(dto.getSexo().getNome()));

    }
}
